package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // public List<UserModel> findAll() {
    //          return  this.userRepository.findAll();
    // }

    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers");

        List<UserModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");
            result = this.userRepository.findAll(Sort.by("Age"));
        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("addUser");
        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById");
        System.out.println("Obteniendo el usuario con la id " + id);
        return this.userRepository.findById(id);
    }

    public boolean delete(String id) {
        System.out.println("delete usuario");
        System.out.println("La id del usuario a borrar es " + id);

        boolean result = false;

        if (this.userRepository.findById(id).isPresent() == true) {
             System.out.println("Usuario encontrado. Borrando");
             this.userRepository.deleteById(id);
             result = true;
        }
        return result;
    }

    public UserModel update(UserModel user) {
        System.out.println("update user");
        return this.userRepository.save(user);
    }

}
