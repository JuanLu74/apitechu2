package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    @Id
    private String id;
    private String name;
    private Integer age;

    // Constructor sin parametros
    public UserModel() {
    }

    // Constructor con todos los parametros
    public UserModel(String id, String name, Integer age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    // Getters y Setters
    public String getId() { return this.id; }
    public void setId(String id) { this.id = id; }

    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }

    public Integer getAge() { return this.age; }
    public void setAge(Integer age) { this.age = age; }


}
