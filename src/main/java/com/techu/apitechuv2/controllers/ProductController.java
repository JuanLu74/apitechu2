package com.techu.apitechuv2.controllers;


import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts(){
        System.out.println("getProducts");

        return this.productService.findALL();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
//  public ResponseEntity<?> getProductById(@PathVariable String id) {    --> También vale
        System.out.println("getProducById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

//       Si Result viene vacío, da error por nullexception
//        result.getPrice();
//        result.getAnotherObject().getYetAnotherObject().getProerty()....;
//        if (result.isPresent() == true) {
//            return new ResponseEntity<>(result.get(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
//        }
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.print("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La Descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El Precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct (@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent() == true) {
            System.out.println("Producto para actualizar eocntrado, actualizando");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

     @DeleteMapping("/products/{id}")
     public ResponseEntity<String> deleteProduct(@PathVariable String id) {
          System.out.println("deleteProduct");
          System.out.println("La id del producto a borrar es " + id);

          boolean deletedProduct = this.productService.delete(id);

          return new ResponseEntity<>(
                 deletedProduct ? "producto borrado" : "Producto no borrado",
                 deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
         );

     }




}
