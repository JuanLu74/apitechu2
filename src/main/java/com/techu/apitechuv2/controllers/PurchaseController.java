package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.services.ProductService;
import com.techu.apitechuv2.services.PurchaseService;
import com.techu.apitechuv2.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id de la compra a añadir es " + purchase.getId());
        System.out.println("La id del usuario de la compra es " + purchase.getUserId());
        System.out.println("La cantidad de objetos a comprar es " + purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getHttpStatus());
    }

}
